package com.example.loginscreen

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class LogInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun setUp() {
        emailText.setText(intent.extras!!.getString("email", ""))
        passText.setText(intent.extras!!.getString("password", ""))
    }

    fun goBack() {
        val intent = Intent()
        intent.putExtra("jemali", "jemali aris kai bichi")
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun saveData() {
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("email", emailText.text.toString())
        edit.putString("password", passText.text.toString())
        edit.commit()
    }

    private fun readData() {
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        emailText.setText(sharedPreferences.getString("email", ""))
        passText.setText(sharedPreferences.getString("password", ""))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(resultCode == 11){
                Toast.makeText(this,data!!.extras!!.getString("jemali",""),Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        logInButt.setOnClickListener() {
            if (emailText.text.isNotEmpty() && passText.text.isNotEmpty()) {
                logIn()
            }
        }
    }

    private fun logIn() {
        auth.signInWithEmailAndPassword(emailText.text.toString(), passText.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("signIn", "signInWithEmail:success")
                    val user = auth.currentUser
                    val intent = Intent(this, ProfileActivity::class.java)
                    startActivity(intent)
                    saveData()
                    val intent2 = Intent(this,pr)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("signIn", "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

}
