package com.example.loginscreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main2.*

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        init()
    }
    private fun init()
    {
        Glide.with(this)
            .load("https://scontent.ftbs5-2.fna.fbcdn.net/v/t1.0-9/27750143_1594417797315835_2175692300733643966_n.jpg?_nc_cat=109&_nc_ohc=aOsu8DsBNEsAQn1Ks_TqLAlZJ-RzvnaEFFi6UaXUdIuOu6rbPdJmMQ0oQ&_nc_ht=scontent.ftbs5-2.fna&oh=6a61c5c850aa1ccbc6df258fe8b37e63&oe=5E81B9F5")
            .placeholder(R.mipmap.ic_launcher).into(imageView1)
        Glide.with(this)
            .load("https://scontent.ftbs5-1.fna.fbcdn.net/v/t1.0-9/64636394_2253375511420057_5112104692582711296_n.jpg?_nc_cat=102&_nc_ohc=yXFLcH-GgxMAQlARgfMXImnpIBgdNpP4_8dziM5q_7AtKA5wp4ruXVvHQ&_nc_ht=scontent.ftbs5-1.fna&oh=bca371491ec12d225081f4f2a89ad2e1&oe=5E6DCD95")
            .placeholder(R.mipmap.ic_launcher).into(imageView2)
    }
}
